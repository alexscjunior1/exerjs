// GERAR ARRAY ALEATÓRIO
function gerarArray() {
    let result = [];
    for (let i = 0; i < 100; i++) {
        result.push(Math.floor(Math.random()*100));
    }
    return result;
}

// ORDENAÇÃO DO ARRAY
function ordenarArray(array) {
    let tamanho = array.length;
    while (tamanho > 0) {
        let maiorValor = 0;
        for (let i = tamanho - 1; i > -1; i--) {
            if (array[i] > maiorValor) {
                maiorValor = array[i];
                let temp = array[i];
                array[i] = array[tamanho - 1];
                array[tamanho - 1] = temp;
            }
        }
        tamanho --;
    }
}

// PROGRAMA PRINCIPAL
let exemplo = gerarArray();
console.log(exemplo);
ordenarArray(exemplo);
console.log(exemplo);